(function() {
  var cp = clientProbe(window, {
    initOnLoad: false
  });

  document.addEventListener('DOMContentLoaded', function() {
    cp.init();
  });

  document.getElementById('btnClearFlags').addEventListener('click', function() {
    cp.clearFlags();
  });
})();
