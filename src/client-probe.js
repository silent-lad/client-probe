/**
 * client-probe v0.1
 * 
 * Author: Kushal Pandya <kushalspandya@gmail.com> (https://doublslash.com)
 * Date: Sept 1, 2018
 * 
 * Main Client Probe Script
 */

(function() {
  'use strict';
  
  var self = this;

  var platformMatchers = {
    WINDOWS: /windows/ig,
    MACOS: /macintosh/ig,
    LINUX: /linux/ig,
    ANDROID: /android/ig,
    IOS: [/ipad/ig, /iphone/ig],
  };

  var browserMatchers = {
    CHROME: [/chrome/ig, /crios/ig],
    FIREFOX: [/firefox/ig, /fxios/ig],
    SAFARI: /safari/ig,
    OPERA: /opr/ig,
    EDGE: [/edge/ig, /edgios/ig, /edga/ig],
    IE: /trident/ig,
  };

  var _extend = function() {
    var key, i;

    for (i = 1; i < arguments.length; i++)
    {
      for (key in arguments[i])
      {
        if (arguments[i].hasOwnProperty(key))
          arguments[0][key] = arguments[i][key];
      }
    }

    return arguments[0];
  };

  var _probe = function(userAgent, matchers) {
    var matches = {};

    for (var candidate in matchers) {
      if (!Array.isArray(matchers[candidate])) {
        matches[candidate] = matchers[candidate].test(userAgent);
      } else {
        for(var i = 0; i < matchers[candidate].length; i++) {
          if (!matches[candidate])
            matches[candidate] = matchers[candidate][i].test(userAgent);
        }
      }
    }

    return matches;
  };

  var _normalize = function(platform, browser) {
    // Fix WebKit/Blink browsers reporting as Safari on Windows
    if (platform.WINDOWS) {
      browser.SAFARI = false;

      // Fix Edge reporting as Chrome
      if (browser.EDGE) {
        browser.CHROME = false;
      }
    }

    // Fix Chrome reporting as Safari on macOS
    if (platform.MACOS && browser.CHROME) {
      browser.SAFARI = false;
    }

    // Fix Android reporting as Linux and
    // Chrome, Edge and Opera reporting as Safari
    if (platform.ANDROID &&
        (browser.CHROME || browser.EDGE || browser.OPERA)) {
      platform.LINUX = false;
      browser.SAFARI = false;
    }

    return {
      platform: platform,
      browser: browser
    };
  };

  var _getCssFlags = function(matches, flagPrefix) {
    var flags = [];
    for (var match in matches) {
      if (matches[match]) {
        flags.push(flagPrefix + match.toLowerCase());
      }
    }

    return flags.join(' ');
  };

  var _getJsFlags = function(matches, flagPrefix) {
    var flags = {};
    for (var match in matches) {
      var flagName = flagPrefix + match.charAt(0).toUpperCase() + match.toLowerCase().slice(1);
      flags[flagName] = matches[match] || false;
    }
    return flags;
  };

  /**
   * Main clientProbe Object Definition
   *
   * @param {object} client (Must be a reference to Window or equivalent)
   * @param {string} userConfig (Must be a string representing client user-agent)
   */
  var clientProbe = function(client, userConfig) {
    var documentBody = client.document.body;
    var defaultConfig = {};
    var thisClientProbe = {};
    var originalAttributeVal;
    var platformJsFlags;
    var browserJsFlags;

    /**
     * Default clientProbe config
     */
    defaultConfig = {
      initOnLoad: true,
      enableCssFlags: true,
      enableJsFlags: true,
      cssFlagOptions: {
        browserFlagPrefix: 'browser-',
        platformFlagPrefix: 'platform-',
        flagAttribute: 'class',
      },
      jsFlagOptions: {
        browserFlagPrefix: 'isBrowser',
        platformFlagPrefix: 'isPlatform',
        flagParent: client,
      }
    };

    // Perform overrides with user config
    userConfig = _extend(defaultConfig, userConfig);

    /**
     * Method for manual flag initialization.
     *
     * Useful if user config has `initOnLoad` set to `false`
     */
    thisClientProbe.init = function() {
      var probeResults = _normalize(
        _probe(client.navigator.userAgent, platformMatchers),
        _probe(client.navigator.userAgent, browserMatchers)
      );

      // Check if config demands CSS flags
      if (userConfig.enableCssFlags) {
        // Get Platform Flags
        var platformFlag = _getCssFlags(
          probeResults.platform,
          userConfig.cssFlagOptions.platformFlagPrefix
        );

        // Get Browser Flags
        var browserFlag = _getCssFlags(
          probeResults.browser,
          userConfig.cssFlagOptions.browserFlagPrefix,
        );

        originalAttributeVal = documentBody.getAttribute(userConfig.cssFlagOptions.flagAttribute) || '';
        documentBody.setAttribute(
          userConfig.cssFlagOptions.flagAttribute,
          originalAttributeVal + ' ' + platformFlag + ' ' + browserFlag
        );
      }

      // Check if config demands JS flags
      if (userConfig.enableJsFlags) {
        // Get Platform FLags
        platformJsFlags = _getJsFlags(
          probeResults.platform,
          userConfig.jsFlagOptions.platformFlagPrefix
        );

        // Get Browser Flags
        browserJsFlags = _getJsFlags(
          probeResults.browser,
          userConfig.jsFlagOptions.browserFlagPrefix,
        );

        _extend(userConfig.jsFlagOptions.flagParent, platformJsFlags, browserJsFlags);
      }
    };

    /**
     * Method to clear all the flags set by clientProbe
     */
    thisClientProbe.clearFlags = function() {
      // Clear all CSS Flags
      documentBody.setAttribute(userConfig.cssFlagOptions.flagAttribute, originalAttributeVal);

      // Clear all JS flags
      var flags = _extend({}, platformJsFlags, browserJsFlags);
      for(var flag in flags) {
        delete userConfig.jsFlagOptions.flagParent[flag];
      }
    };

    if (userConfig.initOnLoad) {
      thisClientProbe.init();
    }

    return thisClientProbe;
  };

  // UMD Definition < https://github.com/umdjs/umd >.
  if (typeof exports !== 'undefined')
  {
    if (typeof module !== 'undefined' && module.exports)
    {
      exports = module.exports = clientProbe;
    }
    exports.clientProbe = clientProbe;
  }
  else
  {
    self.clientProbe = clientProbe;
  }
}).call(this);